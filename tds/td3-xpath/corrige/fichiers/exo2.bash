#!/usr/bin/env bash

# Sans couleur.
NC='\033[0m'
RESULT='\033[0;33m'
RQ='\033[0;31m'

QFROM=1
QTO=1
FILE="breves-inline.xml"

xpath12="/*/node()"
xpath1="//node()"
xpath2="/descendant-or-self::*"
#xpath3="//@href"
xpath3="//@*[starts-with(.,'http')]"
xpath4="count(//breve)"
xpath5="//breve/titre/text()"
xpath6="//breve[@theme='sport']"
xpath7="//breve[photo]"
xpath8="//breve[starts-with(titre/text(),'D')]"
xpath9="//breve[contains(texte/text(),'marathon')]"
xpath10="//breve[@theme='actu']/following::*"
xpath11="//breve[@theme='actu']/following::*[1]"


# Paramètres exercice / questions.
if [ "$#" -eq 1 ]; then
    QFROM=$1
    QTO=$1
fi

for ((i = $QFROM ; i <= $QTO ; i++))
do

    echo "==============================="
    echo "========= Question $i ========="
    echo "==============================="
    echo ""

    echo -e "${RQ}"
    var="xpath$i"
    varbis=${!var}
    echo $varbis
    echo -e "${RESULT}"
    xmllint -xpath "$varbis" $FILE
    echo -e "${NC}"

done
