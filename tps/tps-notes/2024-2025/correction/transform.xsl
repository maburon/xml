<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml">
    <xsl:output method="html" encoding="UTF-8"/>

    <xsl:template match="/">    
        <html>
            <body>
                <table border="1">
                    <thead>
                        <tr>
                            <th>Carte</th>
                            <th>Identifiant</th>
                            <th>Classe</th>
                            <th>Occurence</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="jeu/cartes/carte">
                            <xsl:sort select="nom" order="descending"/>
                            <tr>
                                <td><xsl:value-of select="nom"/></td>
                                <td><xsl:value-of select="@id"/></td>
                                <td><xsl:value-of select="classe"/></td>
                                <td>
                                    <xsl:call-template name="occurence">
                                        <xsl:with-param name="id">
                                            <xsl:value-of select="@id"/>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
            </body>
        </html>
    </xsl:template>

    <xsl:template name="occurence">
        <xsl:param name="id"/>

        <xsl:value-of select="count(/jeu/decks/deck/carte[text()=$id])"/>

    </xsl:template>

</xsl:stylesheet>
