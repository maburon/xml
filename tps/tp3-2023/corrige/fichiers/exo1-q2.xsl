<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="club">
        <club> 
            <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
            <nom><xsl:value-of select="nom"/></nom>
            <ville><xsl:value-of select="ville"/></ville>
        </club>
    </xsl:template>

    <xsl:template match="/">    
        <clubs>
            <xsl:apply-templates select="championnat/clubs/club"/> 
        </clubs>
    </xsl:template>
</xsl:stylesheet>
