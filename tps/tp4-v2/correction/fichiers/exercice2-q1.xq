declare variable $d := doc('plant_catalog.xml');

declare function local:customCopy($plant as element(PLANT))
as element(PLANT)
{
    <PLANT>
    {
        for $e in $plant//*
        where not($e/name() = 'LIGHT') 
        return
        $e
    }
    </PLANT>
};

<CATALOG>
{
    for $light in distinct-values($d//LIGHT)
    return
    <LIGHT>
        <EXPOSURE>{$light}</EXPOSURE>

        {
            for $p in $d//PLANT[LIGHT=$light]
            return
            local:customCopy($p)
        }
    </LIGHT>
}
</CATALOG>
