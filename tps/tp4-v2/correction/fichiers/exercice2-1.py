#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Importation de l'API.
import xml.etree.ElementTree as ET

# Chemin vers le fichier xml.
FILE = 'championnat.xml'

# Création du parser et récupération de l'arbre XML du document.
root = ET.parse(FILE).getroot()

for child in root.findall('journees/journee'):
    l = child.findall('rencontre')
    if len(l) != 10:
        print('La journée ' + child.attrib['num'] + ' n\'a pas exactement 10 rencontres.')

