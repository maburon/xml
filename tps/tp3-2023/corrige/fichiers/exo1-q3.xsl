<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.01//EN" doctype-system="http://www.w3.org/TR/html4/strict.dtd" indent="yes"/>

    <xsl:template match="club">
        <tr>
            <td><xsl:value-of select="ville"/></td>
            <td><xsl:value-of select="nom"/></td>
        </tr>
    </xsl:template>

    <xsl:template match="/">
        <html>
            <head>
                <title>Exercice 1 -- Question 3</title>
            </head>
            <body>
                <h2>Les clubs de Ligue 1 <br/>
                    saison 2014-2015
                </h2>
                <table border="1">
                    <thead>
                        <tr>
                            <th>ville</th>
                            <th>club</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="championnat/clubs/club">
                            <xsl:sort select="ville"/>
                            <xsl:apply-templates select="."/>
                        </xsl:for-each>
                    </tbody>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>

