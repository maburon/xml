<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml">
    <xsl:output method="html" encoding="UTF-8"/>

    <xsl:template match="/">    
        <html>
            <body>
                <table border="1">
                    <thead>
                        <tr>
                            <th>Colonne 1</th>
                            <th>Colonne 2</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Cellule 1</td>
                            <td>Cellule 2</td>
                        </tr>
                        <tr>
                            <td>Cellule 3</td>
                            <td>Cellule 4</td>
                        </tr>
                    </tbody>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
