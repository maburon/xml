for $serie in doc("collection.xml")//serie
return 
       <serie titre="{$serie/@nom}">
              <nb_tome>{count($serie/tome)}</nb_tome>
       </serie>