#!/bin/bash

RED='\033[1;31m'
RESULT='\033[0;35m'
NC='\033[0m' # No Color
I=1

xmllint --noblanks ../../sujet/fichiers/booker.xml > booker-normalized.xml

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/booker/award[5]/title/text()' booker-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/booker/award[6]/author/text()' booker-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/booker/award[year=2000]/title/text()' booker-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/booker/award[title="Possession"]/author/text()' booker-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/booker/award[author="J M Coetzee"]/title/text()' booker-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/booker/award[year >= 1995]/author/text()' booker-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath 'count(/booker/award)' booker-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))
