for $c in doc('championnat.xml')//club
return
    <club>
        {$c/nom,
        if ($c/ville = "Lille") then $c/ville
        else if ($c/ville = "Rennes") then "Rennes"
        else "Autre ville"
        }

    </club>
