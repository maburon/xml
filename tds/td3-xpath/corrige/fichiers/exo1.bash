#!/usr/bin/env bash

# Sans couleur.
NC='\033[0m'
RESULT='\033[0;33m'
RQ='\033[0;31m'

QFROM=1
QTO=1
FILE="article-inline.xml"

xpath10="/article"
xpath11="/article//attribute::*"
xpath1="/"
xpath2="/descendant::node()"
xpath3="//@*"
xpath4="//text()"
xpath5="//p/text()"
xpath6="//référence[@code='siteMarc']"
xpath7="/descendant::*/child::auteur[position()=2]"
xpath8="//auteur[count(preceding-sibling::auteur)=1]"
xpath9="//auteur[(preceding-sibling::auteur) and not(preceding-sibling::auteur/preceding-sibling::auteur)]"


# Paramètres exercice / questions.
if [ "$#" -eq 1 ]; then
    QFROM=$1
    QTO=$1
fi

for ((i = $QFROM ; i <= $QTO ; i++))
do

    echo "==============================="
    echo "========= Question $i ========="
    echo "==============================="
    echo ""

    echo -e "${RQ}"
    var="xpath$i"
    varbis=${!var}
    echo $varbis
    echo -e "${RESULT}"
    xmllint -xpath "$varbis" $FILE
    echo -e "${NC}"

done
