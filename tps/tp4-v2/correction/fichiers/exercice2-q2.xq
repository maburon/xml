declare variable $cmd := doc('plant_order.xml');

declare function local:getPrice($common as xs:string, $quantity as xs:integer)
as xs:float
{
    for $plant in doc('plant_catalog.xml')//PLANT[COMMON=$common]
    let $price := replace($plant/PRICE/text(), '\$', '')
    return
    xs:float($price) * $quantity
    
};

<price>
{
    sum
    (
        for $c in $cmd//PLANT
        let $x := ($c/COMMON, $c/QUANTITY)
        return
        local:getPrice($x[1], $x[2])
    )
}
</price>


