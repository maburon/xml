for $p in doc("parts.xml")//part[color="Red"]
let $o := doc("orders.xml")//order[partno = $p/partno]
where count($o) >= 10
order by count($o) descending
return
    <important_red_part>
        { $p/description }
        <avg_price> {avg($o/price)} </avg_price>
    </import_red_part>
