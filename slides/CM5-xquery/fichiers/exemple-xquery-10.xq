for $journee in doc('championnat.xml')//journee
let $num := count($journee/rencontre)
return
    <journee> 
        {$journee/@num}
        <num_rencontre>{$num}</num_rencontre>
    </journee>
