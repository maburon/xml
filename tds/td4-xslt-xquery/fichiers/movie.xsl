<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!-- question 4 -->
  <xsl:import href="actor.xsl" />
  <xsl:output method="html" encoding="UTF-8" indent="yes"/>


  <!-- question 1 -->
  <xsl:template match="movie">
    <html>
      <head>
        <title><xsl:value-of select="title"/></title>
      </head>
      <body>
        <h1><xsl:value-of select="title"/> (<xsl:value-of select="year"/>)</h1>
        <img src="{thumb}" style="float:left; max-width: 250px; margin: 0 20px 20px;" />

        <p><xsl:value-of select="plot"/></p>

        <!-- question 2 -->
        <p>
          <b>Genres</b>:<br/>
          <ul>
            <xsl:for-each select="genre">
              <xsl:sort select="."/>
              <li><xsl:value-of select="."/></li>
            </xsl:for-each>
          </ul>
        </p>

        <!-- question 3 -->
        <!-- Si le template pour actor n'est pas défini, alors les deux templates par défaut (voir cours) sont utilisés et la chaîne de caractère de ces éléments est affiché -->
        <div style="clear: both">
          <h2>Acteurs</h2>
          <div class="acteurs" style="display: flex">
            <xsl:apply-templates select="actor[thumb][position() &lt; 4]"/>
          </div>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>

