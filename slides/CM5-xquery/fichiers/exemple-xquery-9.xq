let $championnat := doc('championnat.xml')/championnat
for $club in $championnat/clubs/club
    for $rencontre in $championnat//rencontre
    where $club/@id = $rencontre/clubReceveur
    return $rencontre
