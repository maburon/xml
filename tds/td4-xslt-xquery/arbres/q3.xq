<especes>{
for $espece in distinct-values(//arbre/nom_commun)
let $nombre := count(//arbre[nom_commun=$espece])
order by $nombre descending
return <espece nombre="{$nombre}">{$espece}</espece>}
</especes>