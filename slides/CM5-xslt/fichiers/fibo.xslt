<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" indent="yes"/>

<xsl:template name="Fibon">
    <xsl:param name="n1" select="0"/>
    <xsl:param name="n2" select="1"/>
    <xsl:param name="num"/>

    <xsl:value-of select="$n1"/>

    <xsl:if test="$num > 0">
        <xsl:text>, </xsl:text>
        <xsl:call-template name="Fibon">
            <xsl:with-param name="n1" select="$n2"/>
            <xsl:with-param name="n2" select="$n1 + $n2"/>
            <xsl:with-param name="num" select="$num - 1"/>
        </xsl:call-template>
    </xsl:if>
</xsl:template>

<xsl:template match="/">
    <result>
        <xsl:call-template name="Fibon">
            <xsl:with-param name="num" select="8"/>
        </xsl:call-template>
    </result>
</xsl:template>

</xsl:stylesheet>
