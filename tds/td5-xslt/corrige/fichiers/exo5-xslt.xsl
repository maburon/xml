<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.01//EN" doctype-system="http://www.w3.org/TR/html4/strict.dtd" indent="yes"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>Exercice 5</title>
            </head>
            <body>
                <h1>Auteurs</h1>

                <ul>
                    <xsl:for-each select="booker/award">
                        <li>
                            <xsl:choose>
                                <xsl:when test="author/@gender='m'">
                                    <i><xsl:value-of select="author"/></i>
                                </xsl:when>

                                <xsl:when test="author/@gender='f'">
                                    <b><xsl:value-of select="author"/></b>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="author"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </li>
                    </xsl:for-each>
                </ul>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>

