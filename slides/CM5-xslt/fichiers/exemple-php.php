<?php

// Chargement du XML.
$xml = new DOMDocument;
$xml->load('collection.xml');

// Chargement du XSL.
$xsl = new DOMDocument;
$xsl->load('collection.xsl');

// Configuration du transformateur.
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // Attachement des règles xsl.

// Affichage de la transformation.
echo $proc->transformToXML($xml);

?>
