#!/bin/bash

RED='\033[1;31m'
RESULT='\033[0;35m'
NC='\033[0m' # No Color

xmllint --noblanks exo4-xml.xml > exo4-xml-normalized.xml

printf "${RED}Requête 1${NC}\n"
printf "${RESULT}"
xmllint --xpath '/produits/fruit/producteur/text()' exo4-xml-normalized.xml
printf "${NC}"
printf "\n\n"

printf "${RED}Requête 2${NC}\n"
printf "${RESULT}"
xmllint --xpath '/produits/legume[origine="Espagne"]' exo4-xml-normalized.xml
printf "${NC}"
printf "\n\n"

printf "${RED}Requête 3${NC}\n"
printf "${RESULT}"
xmllint --xpath '/produits/fruit[@type="clementine" and bio]/origine' exo4-xml-normalized.xml
printf "${NC}"
printf "\n\n"

printf "${RED}Requête 4${NC}\n"
printf "${RESULT}"
xmllint --xpath '/produits/descendant::origine[@region="Bretagne"]/../producteur/text()' exo4-xml-normalized.xml
printf "${NC}"
printf "\n\n"



