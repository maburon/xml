for $tome in doc("collection.xml")//tome
let $serie := doc("collection.xml")//serie[tome=$tome]
return 
  <tome numero="{$tome/@numero}" serie="{$serie/@nom}" titre="{$tome/titre/text()}"/>