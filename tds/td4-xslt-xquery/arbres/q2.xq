let $nombre := count(/arbres/arbre)
let $moyenne := avg(//hauteur)
return <html>
  <body>
    Il y a {$nombre} d'arbre de {$moyenne}m de haut en moyenne.
  </body>
</html>