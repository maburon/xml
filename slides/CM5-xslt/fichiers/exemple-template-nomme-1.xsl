<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:output methode="html" encoding="utf-8"/>

<xsl:template name="afficher">
    <xsl:value-of select="position()"/> : <xsl:value-of select="."/>
</xsl:template>

<xsl:template match="/livres/*">
    <xsl:call-template name="afficher"/>
</xsl:template>

<xsl:template match="text()">
    <xsl:call-template name="afficher"/>
</xsl:template>

<xsl:template name="afficher-message">
    <xsl:param name="message" select="'vide'"/>
    <xsl:value-of select="position()"/> : <xsl:value-of select="$message"/>
</xsl:template>

<xsl:template match="*">
    <xsl:call-template name="afficher-message">
        <xsl:with-param name="message" select="'Nœud élément'"/>
    </xsl:call-template>
</xsl:template>

</xsl:stylesheet>
