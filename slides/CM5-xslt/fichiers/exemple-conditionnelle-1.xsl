<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output methode="html" encoding="utf-8"/>

<xsl:template match="film">
    <xsl:if test="annee > 1970">
        <xsl:copy-of select="."/>
    </xsl:if>
</xsl:template>

<xsl:choose>
    <xsl:when test="annee mod 4">non</xsl:when>
    <xsl:when test="annee mod 100">oui</xsl:when>
    <xsl:when test="annee mod 400">peut-être</xsl:when>
    <xsl:otherwise>par défaut</xsl:otherwise>
</xsl:choose>

<xsl:value-of select="count(a)"/> item<xsl:if test="count(a) > 1">s</xsl:if>

</xsl:stylesheet>
