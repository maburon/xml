<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output methode="html" encoding="utf-8"/>

<xsl:template match="livre">
    <ul><xsl:apply-templates select="auteurs/nom"/></ul>
</xsl:template>

<xsl:template match="nom">
    <li><xsl:value-of select="."/></li>
</xsl:template>

</xsl:stylesheet>
