#!/bin/bash

RED='\033[1;31m'
RESULT='\033[0;35m'
NC='\033[0m' # No Color
I=1

xmllint --noblanks ../../sujet/fichiers/contacts.xml > contacts-normalized.xml

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/Contacts' contacts-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/Contacts/Person' contacts-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '//Person[Firstname="John"]' contacts-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '//Person[Email]' contacts-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/Contacts/Person[1]/Firstname/child::text()' contacts-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/Contacts/Person[1]/Firstname/text()' contacts-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/Contacts//Address[@type="home"]//Street/child::text()' contacts-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/Contacts//Address[@type="home" and City="London"]' contacts-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/Contacts//Address[@type="work" and City="Dublin"]/parent::node()/Lastname/text()' contacts-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/Contacts//Address[@type="work" and City="Dublin"]/../Lastname/text()' contacts-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/Contacts[.//Address[@type="work" and City="Dublin"]]//Lastname/text()' contacts-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/Contacts//Address[@type="work"]/ancestor::node()' contacts-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/Contacts/Person[Lastname="Smith"]/following-sibling::node()/Lastname/text()' contacts-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/Contacts/Person[following-sibling::node()/Lastname="Dunne"]/Lastname/text()' contacts-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

