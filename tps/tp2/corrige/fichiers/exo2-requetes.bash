#!/bin/bash

RED='\033[1;31m'
RESULT='\033[0;35m'
NC='\033[0m' # No Color
I=1

xmllint --noblanks ../../sujet/fichiers/cd.xml > cd-normalized.xml

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/CDlist/CD/performance/composition/text()' cd-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/CDlist/CD/performance[soloist]/composition/text()' cd-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/CDlist/CD/performance[not(soloist) and count(orchestra)=1]' cd-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/CDlist/CD/performance[orchestra="London Symphony Orchestra" and ../publisher="Deutsche Grammophon"]/soloist/text()' cd-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I${NC}\n"
printf "${RESULT}"
xmllint --xpath '/CDlist/CD[performance/orchestra="London Symphony Orchestra"]' cd-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))


