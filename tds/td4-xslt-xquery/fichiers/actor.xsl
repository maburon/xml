<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="html" encoding="UTF-8" indent="yes"/>

  <!-- question 6 -->
  <xsl:variable name="img-width">150</xsl:variable>
  
  <xsl:template name="image">
    <xsl:param name="url" />
    <img style="{concat('width:', $img-width, 'px')}">
      <!-- manière alternative d'ajouter un attribut -->
      <xsl:attribute name="src">
        <xsl:value-of select="$url"/>
      </xsl:attribute>
    </img>
  </xsl:template>
  
  <xsl:template match="actor">
    <div class="actor" style="margin:5px">
      <xsl:call-template name="image">
        <xsl:with-param name="url" select="thumb"/>
      </xsl:call-template>
      <!-- question 5 -->
      <p>
        <xsl:choose>
          <xsl:when test="profile">
            <a href="{profile}"><xsl:value-of select="name"/></a>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="name"/>
          </xsl:otherwise>
        </xsl:choose>
      </p>
    </div>
  </xsl:template>
</xsl:stylesheet>

