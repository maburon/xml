<arrondissements>{
for $arr in distinct-values(//arbre/lieu/adresse/@arrondissement)
let $arr_arbre := //arbre[lieu/adresse/@arrondissement=$arr]
let $arr_nb := count($arr_arbre)
order by $arr_nb descending
return <arrondissement nom="{$arr}" nombre="{$arr_nb}">
{
  for $genre in distinct-values($arr_arbre//genre)
  let $genre_nb := count($arr_arbre[genre=$genre])
  order by $genre_nb descending
    return <genre nom="{$genre}" nb="{$genre_nb}"/>
}
</arrondissement>}
</arrondissements>