from xml.sax import parse
from xml.sax.handler import ContentHandler

class FactbookContentHandler2(ContentHandler):

    def __init__(self):
        # l'état peut être soit INIT, PAYS ou LANG
        # suivant l'emplacement du document dans lequel on se situe
        self.etat = None
        self.pays = None
        self.langue = ""
        parse("factbook.xml", self)

    def startDocument(self):
        self.etat = "INIT"

    def startElement(self, name, attrs):
        match self.etat:
            case "INIT":
                if name == "country":
                    # mémoriser le nom du pays
                    self.pays = attrs.getValue("name")
                    self.etat = "PAYS"
            case "PAYS":
                if name == "languages" :
                    self.etat = "LANG"

    def endElement(self, name):
        match self.etat :
            case "LANG":
                if name == "languages":
                    if self.pays is not None :
                        # imprimer sans sauter de ligne
                        print(self.pays + " : ", end="")
                        self.pays = None
                    else:
                        print(", ", end="")
                    print(self.langue, end="")
                    self.langue = ""
                    self.etat = "PAYS"
            case "PAYS":
                if name == "country":
                    if self.pays is None :
                        print("")
                    self.etat = "INIT"

    def characters(self, content):
        if self.etat == "LANG":
            self.langue += content

FactbookContentHandler2()
