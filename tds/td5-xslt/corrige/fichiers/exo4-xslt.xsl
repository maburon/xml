<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.01//EN" doctype-system="http://www.w3.org/TR/html4/strict.dtd" indent="yes"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>Exercice 3</title>
            </head>
            <body>
                <xsl:variable name="min-prix" select="65"/>
                <h1>Auteurs d'un livre ayant un prix > <xsl:value-of select="$min-prix"/>€</h1>

                <ul>
                    <xsl:for-each select="booker/award">
                        <xsl:if test="title/@price > $min-prix">
                            <li><xsl:value-of select="author"/></li>
                        </xsl:if>
                    </xsl:for-each>
                </ul>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>

