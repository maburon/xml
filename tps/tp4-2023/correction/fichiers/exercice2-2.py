#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Importation de l'API.
import xml.etree.ElementTree as ET

# Chemin vers le fichier xml.
FILE = 'championnat.xml'

# Création du parser et récupération de l'arbre XML du document.
root = ET.parse(FILE).getroot()


# On récupère le sous-arbre des journees.
subroot = root.find('journees')

# Pour chaque journee dans journees, on supprime l'élément score.
for rencontre in subroot.findall('journee/rencontre'):
    score = rencontre.find('score')
    rencontre.remove(score)

# Écriture du nouveau document XML.
subtree = ET.ElementTree(subroot)
subtree.write('exercice2-2.xml')
