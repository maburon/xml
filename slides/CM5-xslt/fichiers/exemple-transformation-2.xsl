<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html" encoding="utf-8"/>
<xsl:template match="/">
    <html>
        <head>
            <title>
                <xsl:value-of select="livre/titre"/>
            </title>
        </head>
        <body>
            <!-- select par defaut : child::node() -->
            <xsl:apply-templates/>
        </body>
    </html>
</xsl:template>

<xsl:template match="livre">
    Titre : <xsl:value-of select="titre"/>

    <h2>Liste des auteurs</h2>
    <ul>
        <xsl:apply-templates select="auteurs/nom"/>
    </ul>
</xsl:template>

<xsl:template match="nom">
    <li><xsl:value-of select="."/></li>
</xsl:template>

</xsl:stylesheet>
