declare variable $id := 3;
declare variable $nom := "Jean";
declare variable $travail := <travail>Astronaute</travail>;
declare variable $base := 5000;

<employe id="{$id}">
    <nom>{$nom}</nom>
    {$travail}
    <salaire>{$base + 1000}</salaire>
</employe>
