<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


    <xsl:output method="html" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="DTD/xhtml1-strict.dtd" indent="yes"/> 


    <xsl:template match="/actu">

        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Actualités</title>
                <meta name="robots" content="noindex" />
                <link rel="stylesheet" type="text/css" href="actu_fichiers/actu.css" />
            </head>
            <body>
                <div class="pagelarge">
                    <h1>Toute l'actualité</h1>

                    <p>
                        Cette page est produite dans le cadre d'exercices sur XSLT. Son contenu n'est peut-être pas pertinent.
                    </p>
                    <ul>
                        <li>retour aux <a href="https://fabien-torre.fr/Enseignement/tp/XML/Corrections/#xsltbreves">solutions des exercices XML</a></li>
                        <li>retour aux <a href="https://fabien-torre.fr/Enseignement/tp/XML/XSLT/">exercices XSLT</a></li>
                        <li>retour à la page de <a href="https://fabien-torre.fr/">Fabien Torre</a></li>
                    </ul>

                    <h2>Statistiques sur les brèves du site</h2>

                    <ul>
                        <li><xsl:value-of select="count(breve[@langue='fr'])"/> brèves en français ;</li>
                        <li><xsl:value-of select="count(breve[@langue='en'])"/> en anglais.</li>
                    </ul>	


                    <h2>A la Une</h2>
                    <xsl:apply-templates select="breve[@theme='actu']"/>

                    <h2>Actualité du sport</h2>
                    <xsl:apply-templates select="breve[@theme='sport']"/>

                    <h2>Actualité de la finance</h2>
                    <xsl:apply-templates select="breve[@theme='bourse']"/>

                    <h2>Actualité des médias</h2>
                    <xsl:apply-templates select="breve[@theme='media']"/>

                </div>
            </body>
        </html>

    </xsl:template>





    <xsl:template match="breve">

        <div class="encart">

            <h1>
                <xsl:value-of select="titre"/>
                (<xsl:value-of select="@date"/>)
            </h1>

            <xsl:apply-templates select="photo"/>

            <p>
                <xsl:value-of select="texte"/>
            </p>

            <ul>
                <xsl:apply-templates select="url"/>
            </ul>

        </div>
    </xsl:template>



    <xsl:template match="photo">
        <img style="float:left" src="{@src}"/>
    </xsl:template>


    <xsl:template match="url">
        <li><a href="{@href}"><xsl:value-of select="."/></a></li>
    </xsl:template>


</xsl:stylesheet>
