from xml.sax import parse
from xml.sax.handler import ContentHandler

class FactbookContentHandler1(ContentHandler):
    
    def __init__(self):
        parse("factbook.xml", self);
    
    def startElement(self, name, attrs):
        if name == "mountain":
            if attrs.getValue("name") == "Mount Everest":
                print("Le mont Everest mesure " + attrs.getValue("height") + "m d'altitude") 
    
    def endElement(self, name):
        return

FactbookContentHandler1()
