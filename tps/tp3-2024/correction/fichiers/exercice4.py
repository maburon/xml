#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Importation de l'API.
import xml.etree.ElementTree as ET

# Chemin vers le fichier xml.
FILE = 'championnat.xml'

# Création du parser et récupération de l'arbre XML du document.
root = ET.parse(FILE).getroot()


# On récupère le sous-arbre des journees.
subroot = root.find('journees')

# On peut supprimer les journées de l'arbre à écrire en XML.
root.remove(subroot)

# Retourne la liste des clubs.
def get_list_club(r):
    # La liste des IDs que l'on va mettre à jour au fur et à mesure.
    l = []

    # On va itérer sur tous les clubs.
    for c in r.findall('clubs/club'):

        # On ajoute l'id trouvé dans la liste.
        l.append(c.attrib['id'])

    return l

# Ajoute la rencontre à la racine.
def ajouter_rencontre(rencontre, r):
    newRencontre = ET.SubElement(r, 'rencontre')
    ET.SubElement(newRencontre, 'clubReceveur').text = rencontre.find('clubReceveur').text
    ET.SubElement(newRencontre, 'clubInvite').text = rencontre.find('clubInvite').text
    ET.SubElement(newRencontre, 'score').text = rencontre.find('score').text

# On récupère la liste des clubs.
clubList = get_list_club(root)

for club in clubList:
    # Ajout d'une balise rencontres au club.
    currentClub = root.find('clubs/club[@id=\'' + str(club) + '\']')
    rencontres = ET.SubElement(currentClub, 'rencontresReceveur')

    for rencontre in subroot.findall('journee/rencontre[clubReceveur=\'' + str(club) + '\']'):
        ajouter_rencontre(rencontre, rencontres)

# Écriture du nouveau document XML.
tree = ET.ElementTree(root)
tree.write('exercice4.xml')
