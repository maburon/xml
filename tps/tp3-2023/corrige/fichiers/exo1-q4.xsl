<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" encoding="UTF-8"/>

    <xsl:template match="/">
        <xsl:call-template name="get-journee">
            <xsl:with-param name="n" select="18"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="get-journee">
        <xsl:param name="n"/>
        <xsl:copy-of select="championnat/journees/journee[@num=$n]"/>
    </xsl:template>

</xsl:stylesheet>

