<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output methode="html" encoding="utf-8"/>

<xsl:template match="personne">
    <xsl:for-each select="enfant">
        <xsl:sort select="@age" order="ascending" data-type="number"/>

        <xsl:value-of select="nom"/> : <xsl:value-of select="@age"/>
    </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
