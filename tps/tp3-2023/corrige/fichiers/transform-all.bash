#!/bin/bash

path="../../sujet/fichiers/"

for i in {1..2}
do
    xsltproc -o exo1-q$i.xml exo1-q$i.xsl $path/championnat.xml
    xmllint --format exo1-q$i.xml --output exo1-q$i.xml
done

xsltproc -o exo1-q3.html exo1-q3.xsl $path/championnat.xml
tidy -imq exo1-q3.html 2> /dev/null

for i in {4..6}
do
    xsltproc -o exo1-q$i.xml exo1-q$i.xsl $path/championnat.xml
    xmllint --format exo1-q$i.xml --output exo1-q$i.xml
done

for i in {1..3}
do
    xsltproc -o exo2-q$i.xml exo2-q$i.xsl $path/plant_catalog.xml
    xmllint --format exo2-q$i.xml --output exo2-q$i.xml
done


