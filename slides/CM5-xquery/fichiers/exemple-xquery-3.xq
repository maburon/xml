for $journee in doc('championnat.xml')//journee
let $date := $journee/date/text()
where $journee/@num < 5
order by $date descending
return
<journee>
    {$journee/@num}
</journee>
