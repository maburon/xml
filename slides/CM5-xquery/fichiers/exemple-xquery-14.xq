declare function local:somme($x as xs:integer, $y as xs:integer) as xs:integer
{
    let $r := $x + $y
    return $r
};

local:somme(1, 2)
