let $min := min(for $arbre1 in //arbre return $arbre1/hauteur)
for $arbre in //arbre
where $arbre/hauteur = $min
return $arbre
