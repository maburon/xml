declare variable $document := 'maisons.xml';
<html>
    <head>
        <title>Exercice 1</title>
    </head>

    <body>
    
        <table border="1">
            <thead>
                <tr>
                    <th>Maisons</th>
                    <th>Surface (m2)</th>
                </tr>
            </thead>
            <tbody>
            {
                for $m in doc($document)/maisons/maison
        
                return
                <tr>
                    <td>{concat('Maison ', $m/@id)}</td>
                    <td>{sum($m//@surface-m2)}</td>
                </tr>

            }
            </tbody>
        </table>
    </body>
</html>
