#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Importation de l'API.
import xml.etree.ElementTree as ET

# Chemin vers le fichier xml.
FILE = 'championnat.xml'

# Création du parser et récupération de l'arbre XML du document.
root = ET.parse(FILE).getroot()

# Retourne la liste des clubs.
def get_list_club(r):
    # La liste des IDs que l'on va mettre à jour au fur et à mesure.
    l = []

    # On va itérer sur tous les clubs.
    for c in r.findall('clubs/club'):

        # On ajoute l'id trouvé dans la liste.
        l.append(c.attrib['id'])

    return l

# Pour un club donné, retourne l'équipe contre lequel les club a le plus marqué.
def get_best_enemy(club, r):
    maxClub = club
    scoreMax = 0

    for rencontre in r.findall('journees/journee/rencontre'):
        clubReceveur = rencontre.find('clubReceveur').text
        clubInvite = rencontre.find('clubInvite').text
        scores = rencontre.find('score').text.split()
        score1 = scores[0]
        score2 = scores[1]

        # Si le club est receveur, il faut regarder score1.
        if club == clubReceveur:
            if score1 > scoreMax:
                scoreMax = score1
                maxClub = clubInvite

        # Si le club est invité, il faut regarder score2.
        if club == clubInvite:
            if score2 > scoreMax:
                scoreMax = score2
                maxClub = clubReceveur

    return maxClub

# Récupération de la liste des clubs.
clubList = get_list_club(root)

# Création d'un dictionnaire vide.
butDict = {}

# Création d'un dictionnaire ayant comme clefs les IDs des clubs et comme
# valeur un couple d'entiers (x, y) où x est le nombre de buts marqués
# et y le nombre de buts reçus.
for c in clubList:
    butDict[c] = [0, 0]


# On itère sur chaque rencontre.
for rencontre in root.findall('journees/journee/rencontre'):
    clubReceveur = rencontre.find('clubReceveur').text
    clubInvite = rencontre.find('clubInvite').text
    scores = rencontre.find('score').text.split()
    score1 = scores[0]
    score2 = scores[1]

    # Mise à jour du dictionnaire.
    #print(butDict[clubReceveur])
    butDict[clubReceveur][0] += int(score1)
    butDict[clubReceveur][1] += int(score2)

    butDict[clubInvite][0] += int(score2)
    butDict[clubInvite][1] += int(score1)


for c in clubList:
    bestEnemy = get_best_enemy(c, butDict, root)
    print('Le club ' + c + ' a pour marqué le plus de but contre ' + str(bestEnemy))
