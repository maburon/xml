#!/bin/bash

RED='\033[1;31m'
RESULT='\033[0;35m'
NC='\033[0m' # No Color
I=1

file="iTunes-Music-Library"
xmllint --noblanks ../../sujet/fichiers/$file.xml > $file-normalized.xml

printf "${RED}Requête $I -- %s.xml${NC}\n" "$file"
printf "${RESULT}"
xmllint --xpath 'count(/plist/dict/dict/key)' $file-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I -- %s.xml${NC}\n" "$file"
printf "${RESULT}"
xmllint --xpath '/plist/dict/dict/dict/key[. = "Album"]/following-sibling::string[1]' $file-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I -- %s.xml${NC}\n" "$file"
printf "${RESULT}"
xmllint --xpath '/plist/dict/dict/dict/key[. = "Genre"]/following-sibling::string[1]' $file-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I -- %s.xml${NC}\n" "$file"
printf "${RESULT}"
xmllint --xpath 'count(/plist/dict/dict/dict/key[. = "Genre"]/following-sibling::string[1 and text() = "Jazz"])' $file-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I -- %s.xml${NC}\n" "$file"
printf "${RESULT}"
xmllint --xpath '(/plist/dict/dict/dict/key[. = "Genre"]/following-sibling::string[1])[not(text() = following::text())]' $file-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I -- %s.xml${NC}\n" "$file"
printf "${RESULT}"
xmllint --xpath '/plist/dict/dict/dict/key[. = "Play Count"]/following-sibling::integer[1][. > 0]/../key[. = "Name"]/following-sibling::string[1]' $file-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I -- %s.xml${NC}\n" "$file"
printf "${RESULT}"
xmllint --xpath '/plist/dict/dict/dict[not(key="Play Count")]/key[. = "Name"]/following-sibling::string[1]' $file-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I -- %s.xml${NC}\n" "$file"
printf "${RESULT}"
xmllint --xpath '/plist/dict/dict/dict[key="Year" and not(integer[preceding-sibling::key[1]="Year"] > ../dict/integer[preceding-sibling::key[1]="Year"])]/key[. = "Name"]/following-sibling::string[1]' $file-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))
