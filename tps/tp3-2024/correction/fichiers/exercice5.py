#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Importation de l'API.
import xml.etree.ElementTree as ET
from datetime import datetime

# Chemin vers le fichier xml.
FILE = 'championnat.xml'

# Création du parser et récupération de l'arbre XML du document.
root = ET.parse(FILE).getroot()

def is_pair_correct(journee1, journee2):
    id1 = int(journee1.attrib['num'])
    id2 = int(journee2.attrib['num'])

    date1 = journee1.find('date').text
    date2 = journee2.find('date').text

    dateTime1 = datetime.strptime(date1,'%Y-%m-%d')
    dateTime2 = datetime.strptime(date2,'%Y-%m-%d')

    # On vérifie que si id1 est inférieur à id2, alors il en est de même pour
    # la date.
    if (id1 < id2) and (dateTime1 < dateTime2):
        return True
    elif (id2 < id1) and (dateTime2 < dateTime1):
        return True
    else:
        return False


# On récupère le sous-arbre des journees.
subroot = root.find('journees')

# On récupère les journées dans une liste.
listJournees = subroot.findall('journee')

for i in range(0, len(listJournees)):
    for j in range(i+1, len(listJournees)):
        if not(is_pair_correct(listJournees[i], listJournees[j])):
            print('Problème de chronologie entre journée ' + str(i+1) + ' et journée ' + str(j+1))


