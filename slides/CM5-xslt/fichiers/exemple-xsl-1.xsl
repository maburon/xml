<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output methode="html" encoding="utf-8"/>

<xsl:template match="livre">
    Le titre du livre est : <xsl:value-of select="titre"/>

    <h2>Liste des auteurs</h2>
    <ul>
        <xsl:apply-templates select="auteurs/nom"/>
    </ul>
</xsl:template>

</xsl:stylesheet>
