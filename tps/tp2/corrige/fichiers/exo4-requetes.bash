#!/bin/bash

RED='\033[1;31m'
RESULT='\033[0;35m'
NC='\033[0m' # No Color
I=1

xmllint --noblanks ../../sujet/fichiers/recettes1.xml > recettes1-normalized.xml
xmllint --noblanks ../../sujet/fichiers/recettes2.xml > recettes2-normalized.xml

printf "${RED}Requête $I -- recettes1.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath '/cuisine/recette/titre' recettes1-normalized.xml
printf "${NC}"
printf "\n"
printf "${RED}Requête $I -- recettes2.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath '/cuisine/recette/titre' recettes2-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I -- recettes1.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath '/cuisine/recette/ingredients/ingredient/nom_ing/text()' recettes1-normalized.xml
printf "${NC}"
printf "\n"
printf "${RED}Requête $I -- recettes2.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath '/cuisine/recette/ingredients/ing-recette/@ingredient' recettes2-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I -- recettes1.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath '/cuisine/recette[2]/titre' recettes1-normalized.xml
printf "${NC}"
printf "\n"
printf "${RED}Requête $I -- recettes2.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath '/cuisine/recette[2]/titre' recettes2-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I -- recettes1.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath '/cuisine/recette/texte/etape[last()]' recettes1-normalized.xml
printf "${NC}"
printf "\n"
printf "${RED}Requête $I -- recettes2.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath '/cuisine/recette/texte/etape[last()]' recettes2-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I -- recettes1.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath 'count(/cuisine/recette)' recettes1-normalized.xml
printf "${NC}"
printf "\n"
printf "${RED}Requête $I -- recettes2.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath 'count(/cuisine/recette)' recettes2-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I -- recettes1.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath '/cuisine/recette[count(ingredients/ingredient) < 7]' recettes1-normalized.xml
printf "${NC}"
printf "\n"
printf "${RED}Requête $I -- recettes2.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath '/cuisine/recette[count(ingredients/ing-recette) < 7]' recettes2-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I -- recettes1.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath '/cuisine/recette[count(ingredients/ingredient) < 7]/titre/text()' recettes1-normalized.xml
printf "${NC}"
printf "\n"
printf "${RED}Requête $I -- recettes2.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath '/cuisine/recette[count(ingredients/ing-recette) < 7]/titre/text()' recettes2-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I -- recettes1.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath '/cuisine/recette[ingredients/ingredient/nom_ing="farine"]' recettes1-normalized.xml
printf "${NC}"
printf "\n"
printf "${RED}Requête $I -- recettes2.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath '/cuisine/recette[ingredients/ing-recette/@ingredient="farine"]' recettes2-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))

printf "${RED}Requête $I -- recettes1.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath '/cuisine/recette[categorie="Entrée"]' recettes1-normalized.xml
printf "${NC}"
printf "\n"
printf "${RED}Requête $I -- recettes2.xml${NC}\n"
printf "${RESULT}"
xmllint --xpath '/cuisine/recette[contains(@categ, "entree")]' recettes2-normalized.xml
printf "${NC}"
printf "\n\n"
I=$((I + 1))


