declare variable $bib := doc("biblio.xml");
<data>
{
    for $year in distinct-values($bib//book/@year)
    let $avg := avg($bib//book[@year=$year]/price/text())
    return <year value="{$year}" avgprice="{$avg}"/>
}
</data>

