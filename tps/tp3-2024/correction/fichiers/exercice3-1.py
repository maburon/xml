#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Importation de l'API.
import xml.etree.ElementTree

# Chemin vers le fichier xml.
FILE = 'championnat.xml'

# Création du parseur.
root = xml.etree.ElementTree.parse(FILE).getroot()

# Retourne la liste des clubs.
def get_list_club(r):
    # La liste des IDs que l'on va mettre à jour au fur et à mesure.
    l = []

    # On va itérer sur tous les clubs.
    for c in r.findall('clubs/club'):

        # On ajoute l'id trouvé dans la liste.
        l.append(c.attrib['id'])

    return l

# Cette fonction retourne vrai si la paire ordonnée (id1, id2) est présente
# dans une rencontre.
def is_pair_in_rencontres(id1, id2, parser):

    # On itère sur toutes les rencontres.
    for r in parser.findall('journees/journee/rencontre'):

        # On récupère l'id du club receveur.
        current_id1 = r.find('clubReceveur').text

        # On récupère l'id du club invite.
        current_id2 = r.find('clubInvite').text

        # Si la paire ordonnée passée en paramètre est la même
        # que celle qu'on vient de trouver, on retourne vraie.
        if id1 == current_id1 and id2 == current_id2:
            return True

    # Si on arrive jusqu'ici, c'est que l'on a pas trouvé de paire
    # correspondante. On retourne alors faux.
    return False

def check_every_pair(list_ids, parser):
    for id1 in list_ids:
        for id2 in list_ids:
            if id1 != id2:
                if not is_pair_in_rencontres(id1, id2, parser):
                    print('Paire non présente : ' + id1 + ', ' + id2)


l = get_list_club(root)
check_every_pair(l, root)
