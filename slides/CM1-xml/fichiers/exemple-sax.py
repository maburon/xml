from xml.sax import parse
from xml.sax.handler import ContentHandler

class Evaluator(ContentHandler):
    
    def __init__(self):
        parse("cours.xml", self);
    
    def startDocument(self):
        print( "Début du document" )


    def startElement(self, name, attrs):
        print("Ouverture de la balise " + name)
    
    def endElement(self, name):
        print("Fermeture de la balise " + name)

    
    def endDocument(self):
        print( "Fin du document" )

evaluator = Evaluator()
