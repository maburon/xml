<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.01//EN" doctype-system="http://www.w3.org/TR/html4/strict.dtd" indent="yes"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>Exercice 1</title>
            </head>
            <body>
                <h1>Lauréats Booker Prize</h1>

                <xsl:apply-templates select="booker/award"/>
            </body>
        </html>
    </xsl:template>


    <xsl:template match="award">
        <b>Auteur</b> : <xsl:value-of select="author"/><br/>
        <b>Titre</b> : <xsl:value-of select="title"/><br/>
        <b>Année</b> : <xsl:value-of select="year"/><br/>
        <br/>
    </xsl:template>
</xsl:stylesheet>

